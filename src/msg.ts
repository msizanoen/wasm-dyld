export type Message =
    | {
          eventType: "moduleCreate"
          id: number
          binary: ArrayBuffer
          deps: number[]
          memBase: number
          tableBase: number
          tableSize: number
          gotFunc: Map<string, number>
      }
    | {
          eventType: "gotFunc"
          objectId: number
          name: string
          address: number
      }
