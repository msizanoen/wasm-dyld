import { DynamicLinker } from "./index"
import { Message } from "./msg"

const PAGE_SIZE = 64 * 1024

function makeGotFunc(dyn: DynamicModule) {
    return new Proxy(
        {},
        {
            get(_target, prop: string, _recv) {
                let x = dyn.getGotFunc(prop)
                return x
            }
        }
    )
}

function makeGotMem(dyn: DynamicModule) {
    return new Proxy(
        {},
        {
            get(_target, prop: string, _recv): WebAssembly.Global | number {
                return dyn.getGotMem(prop)
            }
        }
    )
}

function makeEnv(dyn: DynamicModule, orig: any) {
    return new Proxy(orig, {
        get(target: any, prop: string, recv): any {
            try {
                return dyn.resolveFunc(prop)
            } catch (e) {
                return Reflect.get(target, prop, recv)
            }
        }
    })
}

export class DynamicModule {
    memoryBase: number = 0
    instantiating = true
    instance: WebAssembly.Instance
    gotMap: Map<string, WebAssembly.Global> = new Map()
    getGotFunc(name: string): WebAssembly.Global {
        if (this.gotMap.has(name)) {
            return this.gotMap.get(name) as WebAssembly.Global
        }
        let extern = null
        try {
            extern = this.dyld.getExternGot(name)
        } catch (e) {
            //
        }
        if (extern != null) return extern
        for (let dep of this.deps) {
            try {
                return dep.getGotFunc(name)
            } catch (e) {
                // skip
            }
        }
        let global = new WebAssembly.Global({ value: "i32", mutable: true })
        if (this.instantiating) {
            this.gotMap.set(name, global)
            return global
        }
        this.dyld.globalData.lock()
        try {
            this.dyld.restoreMq()
            let idx = this.dyld.table.length
            let func = this.getFunc(name)
            this.dyld.tableSet(idx, func)
            global.value = idx
            this.gotMap.set(name, global)
            this.dyld.event({
                eventType: "gotFunc",
                objectId: this.id,
                name,
                address: idx
            })
        } finally {
            this.dyld.globalData.unlock()
        }
        return global
    }
    gotMemFix: Map<string, WebAssembly.Global> = new Map()
    getGotMem(name: string): WebAssembly.Global | number {
        for (let dep of this.deps) {
            try {
                return dep.getGotMem(name)
            } catch (e) {
                //
            }
        }
        if (this.instantiating) {
            let global = new WebAssembly.Global({
                value: "i32",
                mutable: true
            })
            this.gotMemFix.set(name, global)
            return global
        }
        let mem = this.instance.exports[name]
        if (mem == null) throw new Error(`unresolved symbol ${name}`)
        if (mem instanceof WebAssembly.Global)
            return mem.value + this.memoryBase
        else return mem + this.memoryBase
    }
    getFunc(name: string): Function {
        let func = this.instance.exports[name]
        if (!(func instanceof Function))
            throw new Error(`undefined function ${name}`)
        return func
    }
    resolveFunc(name: string): Function {
        let extern = this.dyld.externFuncs.get(name)
        if (extern != null) return extern
        for (let dep of this.deps) {
            try {
                return dep.resolveFunc(name)
            } catch (e) {
                //
            }
        }
        return this.getFunc(name)
    }
    constructor(
        public id: number,
        public dyld: DynamicLinker,
        moduleBin: ArrayBuffer,
        public deps: DynamicModule[] = [],
        restore: Message | null = null
    ) {
        let module = new WebAssembly.Module(moduleBin)
        let dlsec = WebAssembly.Module.customSections(module, "dylink")
        if (dlsec.length != 1) throw new Error("invalid object")
        let dlbuffer = dlsec[0]
        let dl8 = new Uint8Array(dlbuffer)
        let next = 0
        function getLEB() {
            let ret = 0
            let mul = 1
            while (true) {
                let byte = dl8[next++]
                ret += (byte & 0x7f) * mul
                mul *= 0x80
                if (!(byte & 0x80)) break
            }
            return ret
        }
        let memorySize = getLEB()
        /* let memoryAlign = */ getLEB()
        let tableSize = getLEB()
        /* let tableAlign = */ getLEB()
        let memoryExpand = Math.ceil(memorySize / PAGE_SIZE)
        let memoryBase: number
        let tableBase: number
        if (restore != null) {
            if (restore.eventType != "moduleCreate")
                throw new Error(`internal API violated`)
            tableBase = restore.tableBase
            let tableEnd = tableBase + tableSize
            if (tableEnd > dyld.table.length)
                dyld.table.grow(tableEnd - dyld.table.length)
            memoryBase = restore.memBase
        } else {
            dyld.table.grow(tableSize)
            tableBase = dyld.table.length - tableSize
            if (!dyld.isShared) {
                dyld.memory.grow(memoryExpand)
                memoryBase =
                    dyld.memory.buffer.byteLength - memoryExpand * PAGE_SIZE
            } else {
                memoryBase = dyld.globalData.growPages(memoryExpand) * PAGE_SIZE
            }
        }
        this.memoryBase = memoryBase
        let imports = {
            env: makeEnv(this, {
                __memory_base: memoryBase,
                __stack_pointer: dyld.stackPointer,
                __table_base: tableBase,
                __indirect_function_table: dyld.table,
                memory: dyld.memory
            }),
            "GOT.func": makeGotFunc(this),
            "GOT.mem": makeGotMem(this)
        }
        this.instance = new WebAssembly.Instance(module, imports)
        this.instantiating = false
        let modCreateEvt: Message | null = null
        if (restore != null) {
            for (let fname of restore.gotFunc.keys()) {
                let func = this.getFunc(fname)
                let got = this.gotMap.get(fname)
                if (got == null) throw new Error()
                let idx = restore.gotFunc.get(fname) || 0
                dyld.tableSet(idx, func)
                got.value = idx
            }
        } else {
            let gots = new Map()
            for (let name of this.gotMap.keys()) {
                let func = this.getFunc(name)
                let idx = dyld.table.length
                dyld.tableSet(idx, func)
                this.getGotFunc(name).value = idx
                gots.set(name, idx)
            }
            modCreateEvt = {
                eventType: "moduleCreate",
                id,
                binary: moduleBin,
                deps: deps.map(o => o.id),
                memBase: memoryBase,
                tableBase,
                tableSize,
                gotFunc: gots
            }
        }
        for (let name of this.gotMemFix.keys()) {
            let val = this.gotMemFix.get(name) as WebAssembly.Global
            let gmem = this.getGotMem(name)
            if (gmem instanceof WebAssembly.Global) val.value = gmem.value
            else val.value = gmem
        }
        let tlsInit = this.instance.exports.__wasm_init_tls
        if (tlsInit != null) {
            let tlsSize = this.instance.exports.__tls_size.value
            let pages = Math.ceil(tlsSize / PAGE_SIZE)
            let tlsBase: number
            if (!dyld.isShared) {
                tlsBase = dyld.memory.buffer.byteLength
                dyld.memory.grow(pages)
            } else {
                tlsBase = dyld.globalData.growPages(pages)
            }
            tlsInit(tlsBase)
        }
        if (modCreateEvt != null) dyld.event(modCreateEvt)
    }
}
