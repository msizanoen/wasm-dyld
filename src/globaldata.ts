import { Message } from "./msg"

const ADDR_PAGE_SIZE = 0
const ADDR_LOCK = ADDR_PAGE_SIZE + 4
const ADDR_BASE = ADDR_LOCK + 1
const ADDR32_PAGE_SIZE = 0
const STATE_LOCKED = 1
const STATE_UNLOCKED = 0
const MSG_MODULE_CREATE = 0
const MSG_GOT_FUNC = 1
const MSG_END = 0xff

export class GlobalData {
    cursor = ADDR_BASE
    shared: SharedArrayBuffer
    shared8: Uint8Array
    shared32: Uint32Array
    constructor(buf?: SharedArrayBuffer) {
        if (buf != null) this.shared = buf
        else this.shared = new SharedArrayBuffer(64 * 1024 * 1024) // 64MB
        this.shared8 = new Uint8Array(this.shared)
        this.shared32 = new Uint32Array(this.shared)
        if (buf == null) this.shared8[this.cursor] = MSG_END
    }
    encodeULEB(n: number) {
        do {
            let byte = n & 0x7f
            n = Math.floor(n / 0x80)
            if (n != 0) byte += 0x80
            this.shared8[this.cursor++] = byte
        } while (n != 0)
    }
    decodeULEB(): number {
        let result = 0
        let mul = 1
        while (true) {
            let byte = this.shared8[this.cursor++]
            result += (byte & 0x7f) * mul
            if ((byte & 0x80) == 0) break
            mul *= 0x80
        }
        return result
    }
    newThread(): GlobalData {
        return new GlobalData(this.shared)
    }
    lock() {
        while (
            Atomics.compareExchange(
                this.shared8,
                ADDR_LOCK,
                STATE_UNLOCKED,
                STATE_LOCKED
            )
        ) {
            // spin
        }
    }
    unlock() {
        console.assert(Atomics.load(this.shared8, ADDR_LOCK) == STATE_LOCKED)
        Atomics.store(this.shared8, ADDR_LOCK, STATE_UNLOCKED)
    }
    growPages(n: number): number {
        return Atomics.add(this.shared32, ADDR32_PAGE_SIZE, n)
    }
    writeByte(b: number) {
        this.shared8[this.cursor++] = b
    }
    readByte(): number {
        return this.shared8[this.cursor++]
    }
    encodeStr(s: string) {
        this.encodeULEB(s.length)
        for (let i = 0; i < s.length; i++) {
            this.writeByte(s.charCodeAt(i))
        }
    }
    decodeStr(): string {
        let len = this.decodeULEB()
        let sa = []
        for (let i = 0; i < len; i++) {
            sa.push(this.readByte())
        }
        return String.fromCharCode(...sa)
    }
    decodeMsg(): Message | null {
        switch (this.readByte()) {
            case MSG_END:
                this.cursor -= 1
                return null
            case MSG_GOT_FUNC:
                let address = this.decodeULEB()
                let objectId = this.decodeULEB()
                let name = this.decodeStr()
                return {
                    eventType: "gotFunc",
                    address,
                    objectId,
                    name
                }
            case MSG_MODULE_CREATE:
                let id = this.decodeULEB()
                let memBase = this.decodeULEB()
                let tableBase = this.decodeULEB()
                let tableSize = this.decodeULEB()
                let depsLen = this.decodeULEB()
                let deps = []
                for (let i = 0; i < depsLen; i++) {
                    deps.push(this.decodeULEB())
                }
                let gotFunc = new Map()
                let gotFuncSz = this.decodeULEB()
                for (let i = 0; i < gotFuncSz; i++) {
                    let name = this.decodeStr()
                    let addr = this.decodeULEB()
                    gotFunc.set(name, addr)
                }
                let binLen = this.decodeULEB()
                let bin = new ArrayBuffer(binLen)
                let bin8 = new Uint8Array(bin)
                bin8.set(this.shared8.slice(this.cursor, this.cursor + binLen))
                this.cursor += binLen
                return {
                    eventType: "moduleCreate",
                    binary: bin,
                    id,
                    memBase,
                    tableBase,
                    tableSize,
                    deps,
                    gotFunc
                }
            default:
                throw new Error()
        }
    }
    encodeMsg(m: Message) {
        console.assert(this.readByte() == MSG_END)
        let msgType
        let mtCursor = this.cursor - 1
        switch (m.eventType) {
            case "gotFunc":
                msgType = MSG_GOT_FUNC
                this.encodeULEB(m.address)
                this.encodeULEB(m.objectId)
                this.encodeStr(m.name)
                break
            case "moduleCreate":
                msgType = MSG_MODULE_CREATE
                this.encodeULEB(m.id)
                this.encodeULEB(m.memBase)
                this.encodeULEB(m.tableBase)
                this.encodeULEB(m.tableSize)
                this.encodeULEB(m.deps.length)
                for (let dep of m.deps) this.encodeULEB(dep)
                this.encodeULEB(m.gotFunc.size)
                for (let key of m.gotFunc.keys()) {
                    this.encodeStr(key)
                    this.encodeULEB(m.gotFunc.get(key) as number)
                }
                this.encodeULEB(m.binary.byteLength)
                let bin8 = new Uint8Array(m.binary)
                this.shared8.set(bin8, this.cursor)
                this.cursor += bin8.byteLength
                break
            default:
                throw new Error()
        }
        this.writeByte(MSG_END)
        Atomics.store(this.shared8, mtCursor, msgType)
        this.cursor -= 1
    }
}
