const { DynamicLinker } = require(".")
const fs = require("fs")

let dl = new DynamicLinker(true, 16)
let mod = fs.readFileSync("../testwasm.wasm")
dl.addExtern("printString", "vi", n => {
    let HEAP8 = new Uint8Array(dl.memory.buffer)
    let str = ""
    while (HEAP8[n] != 0) {
        str += String.fromCharCode(HEAP8[n++])
    }
    console.log(str)
})
dl.addExtern("printInt", "vi", console.log)
let loaded = dl.loadDynamic(mod)
let ex = loaded.instance.exports
ex.__wasm_call_ctors()
ex.wasmmain()
console.log(ex.getGx())
console.log(ex.getX())
console.log(ex.getX())
dl.dumpTbl()

let dl2 = new DynamicLinker(true, 16)
dl2.addExtern("printString", "vi", n => {
    let HEAP8 = new Uint8Array(dl2.memory.buffer)
    let str = ""
    while (HEAP8[n] != 0) str += String.fromCharCode(HEAP8[n++])
    console.log(str)
})
dl2.setMemory(dl.memory)
dl2.setMq(dl.globalData.newThread())
dl2.restoreMq()
let ex2 = dl2.modules[loaded.id].instance.exports
dl2.dumpTbl()
ex2.wasmmain()
console.log(ex2.getGx())
console.log(ex2.getX())
console.log(ex2.getX())
dl.restoreMq()
